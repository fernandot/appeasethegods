import QtQuick 2.0
import QtQuick.Controls 1.4

Rectangle {
    id:gameWindow
    color: "transparent"
    border.color: "black"
    border.width: 2
    property int gameStatus: 0;
    property int rage: 1;
    property int bless: 5;
    property int level: (1+lmBunnies.count/10)>3?3:1+lmBunnies.count/10;
    property int points: 0
    property var explosion: new Audio('audio/explosion.wav')
    onLevelChanged: {
        if(level>4) level=4;
    }

    onRageChanged: {
        if(rage>=10)
            gameStatus=2;
        rage=rage>10?10:rage<0?0:rage;
    }
    onBlessChanged: bless=bless>10?10:bless<0?0:bless;

    Component.onCompleted: {
        lmBush.append({itemX:100,itemY:220});
        lmBush.append({itemX:300,itemY:320});
        lmBush.append({itemX:500,itemY:320});
        lmBush.append({itemX:700,itemY:420});
        lmBush.append({itemX:1000,itemY:260});

        lmClouds.append({itemX:Math.random()*20,itemY:Math.random()*100+10});
        lmClouds.append({itemX:Math.random()*20+300,itemY:Math.random()*100+10});
        lmClouds.append({itemX:Math.random()*20+700,itemY:Math.random()*100+10});
        lmClouds.append({itemX:Math.random()*20+1000,itemY:Math.random()*100+10});

        for(var x=0;x<20;x++){
            for(var y=0;y<20;y++){
                if(x%3===0&&y%3===0)
                    lmTrees.append({itemX:Math.random()*200+100*x,itemY:Math.random()*20+imgVulcan.y+imgVulcan.height-100+90*y});
            }
        }
    }

    Image{
        y:imgVulcan.y+imgVulcan.height-100
        z:-10
        width: parent.width/2
        height: 50
        anchors.right: parent.right
        source: "img/horizontalLine.png"
    }
    Image{
        y:imgVulcan.y+imgVulcan.height-100
        z:-10
        width: parent.width/2
        height: 50
        mirror: true
        source: "img/horizontalLine.png"
    }
    Timer{
        id:tmSmoke
        interval:500
        repeat: false
        onTriggered: smoke.visible=false;
    }

    Rectangle{
        id:smoke
        y:10
        anchors.horizontalCenter: rectVolcano.horizontalCenter
        visible: false
        onVisibleChanged: {
            if(visible)
                tmSmoke.start();
        }

        Rectangle{
            x:10-width/2
            anchors.verticalCenter: rectVolcano.verticalCenter
            width: rectVolcano.width-40
            height: rectVolcano.height/2
            radius: width
            color: "red"
            z:1
        }

        Image {
            x:-30
            width: 50
            height: 5
            source: "img/horizontalLine.png"
            rotation: 90
        }
        Image {
            x:0
            y:10
            width: 50
            height: 5
            source: "img/horizontalLine.png"
            rotation: 90
        }
        Image {
            x:30
            width: 50
            height: 5
            source: "img/horizontalLine.png"
            rotation: 90
        }
    }

    ListView{
        model: lmTrees
        delegate: Rectangle{
            x:itemX
            y:itemY
            width: 40
            height: 75
            color: "transparent"
            z:-1
            Image{
                anchors.fill: parent
                source: "img/tree"+(Math.random()*3+1).toString().substr(0,1)+".png"
            }
        }
    }

    Rectangle{
        Row{
            spacing: 5
            Column{
                Text {
                    text: "Gods Rage"
                    font.bold: true
                }
                Rectangle{
                    id:rageBar
                    width: 100
                    height: 10
                    radius: 5
                    color: "grey"
                }
                Text {
                    text: "x"+level
                }
            }
            Column{
                Text {
                    text: "Gods Blessing"
                    font.bold: true
                }
                Rectangle{
                    id:blessBar
                    width: 100
                    height: 10
                    radius: 5
                    color: "grey"
                }
            }
            Text {
                text: "<b>Sacrified Bunnies:</b> "+points
            }
        }
        Rectangle{
            anchors.verticalCenter: rageBar.verticalCenter
            x:rageBar.x
            width: 10*rage
            height: 10
            radius: 5
            color: "red"
        }
        Rectangle{
            y: blessBar.y
            x: 105
            width: 10*bless
            height: 10
            radius: 5
            color: "blue"
        }
    }
    Image {
        id:imgVulcan
        y: 50
        anchors.horizontalCenter: gameWindow.horizontalCenter
        width: 1210/3
        height: 768/3
        source: "img/vulcan.png"
    }
    Rectangle{
        id:rectVolcano
        y: 40
        anchors.horizontalCenter: gameWindow.horizontalCenter
        width: 130
        height: 60
        radius: 5
        color: "transparent"
    }

    ListModel{
        id:lmBunnies
    }
    ListModel{
        id:lmBush
    }
    ListModel{
        id:lmClouds
    }
    ListModel{
        id:lmTrees
    }

    Timer{
        id:tRage
        repeat: true
        running: gameStatus===1
        interval: 5000
        onTriggered: {
            rage+=1+lmBunnies.count/10;
            bless-=1;
        }
    }

    Timer{
        id:tBunnies
        repeat: true
        running: gameStatus===1
        interval: 100000/(bless>0?bless:1);
        triggeredOnStart: true
        onTriggered: {
            for(var i=0;i<lmBush.count;i++)
            {
                lmBunnies.append({itemX:lmBush.get(i).itemX-50+Math.random()*150,itemY:lmBush.get(i).itemY+50+Math.random()*10});
                lmBunnies.append({itemX:lmBush.get(i).itemX-50+Math.random()*150,itemY:lmBush.get(i).itemY+50+Math.random()*10});
            }
        }
    }

    ListView{
        model: lmBunnies
        delegate: Component{
            Rectangle{
                id:rectBunny
                x:itemX
                y:itemY
                width: 30
                height: 30
                color: "transparent"
                property bool mousePressed: false
                onXChanged: if(x>rectVolcano.x&&x<rectVolcano.x+rectVolcano.width
                                    &&y>rectVolcano.y&&y<rectVolcano.y+rectVolcano.height)
                            {
                                bless+=1;
                                rage-=1;
                                smoke.visible=true;
                                points++;
                                explosion.play();
                                lmBunnies.remove(index);
                            }
                onYChanged: if(x>rectVolcano.x&&x<rectVolcano.x+rectVolcano.width
                                    &&y>rectVolcano.y&&y<rectVolcano.y+rectVolcano.height)
                            {
                                bless+=1;
                                rage-=1;
                                smoke.visible=true;
                                points++;
                                explosion.play();
                                lmBunnies.remove(index);
                            }

                MouseArea {
                    anchors.fill: parent
                    onPressed: {
                        parent.mousePressed = true;
                        itemX=parent.x;
                        itemY=parent.y;
                    }
                    onReleased: parent.mousePressed = false;
                    onMouseXChanged: if(parent.mousePressed) parent.x=mouseX+itemX;
                    onMouseYChanged: if(parent.mousePressed) parent.y=mouseY+itemY;
                }
                Image{
                    anchors.fill: parent
                    source: "img/bunny"+(Math.random()*2+1).toString().substr(0,1)+".png"
                }
            }
        }
    }

    ListView{
        model: lmBush
        delegate: Rectangle{
            x:itemX
            y:itemY
            width: 50
            height: 50
            color: "transparent"
            Image{
                anchors.fill: parent
                source: "img/bush1.png"
            }
        }
    }
    ListView{
        model: lmClouds
        delegate: Rectangle{
            x:itemX
            y:itemY
            z:-1
            width: 200
            height: 50
            color: "transparent"
            Image{
                anchors.fill: parent
                source: "img/cloud"+(Math.random()*3+1).toString().substr(0,1)+".png"
            }
        }
    }

    Rectangle{
        anchors.fill: parent
        color: "darkBlue"
        visible: gameStatus===2
        z:10
        Text {
            anchors.centerIn: parent
            text: "Volcano God got too angry and destroyed everthing!"
            color: "white"
        }
    }

    Rectangle{
        color: "white"
        width: 100
        height: 100
        border.color: "black"
        border.width: 2
        radius: 10
        anchors.fill: parent
        visible: gameStatus === 0
        z:10

        Column{
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: 10

            Text {
                anchors.horizontalCenter: parent.horizontalCenter
                text: "Appease the Gods!"
                font.pointSize: 20
                font.bold: true
                color: "darkGreen"
            }

            Rectangle{
                anchors.horizontalCenter: parent.horizontalCenter
                border.color: "black"
                radius: 20
                width: 264/2
                height: 532/2
                Image {
                    anchors.fill: parent
                    source: "img/lemur.png"
                }
            }

            Button{
               anchors.horizontalCenter: parent.horizontalCenter
               text:"Start!"
               onClicked: {
                   gameStatus = 1;
               }
            }
        }
    }
}
